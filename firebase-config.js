import { getFirestore } from "@firebase/firestore"
import { initializeApp } from "firebase/app"
import { getStorage } from "firebase/storage"

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: "todo-99f19.firebaseapp.com",
  projectId: "todo-99f19",
  storageBucket: "todo-99f19.appspot.com",
  messagingSenderId: "834652007321",
  appId: "1:834652007321:web:0dfaf5e0003b59d4ff0108"
}

const app = initializeApp(firebaseConfig)
export const storage = getStorage(app)

export const db = getFirestore(app)