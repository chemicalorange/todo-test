import { useState } from 'react'
import { ref, uploadBytes, getDownloadURL } from "firebase/storage"
import { storage } from 'firebase-config'
import { v4 } from 'uuid'
import styles from './styles.module.css'

/**
 * Component for creating new todo.
 *
 * @component
 * @example
 * return (
 *   <NewTodoForm createTodo={func} closeModal={func} />
 * )
 */

export const NewTodoForm = ({ createTodo, closeModal }) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [date, setDate] = useState('')
  const [filesArray, setFilesArray] = useState([])

  const addTodo = async (e) => {
    e.preventDefault()
    createTodo({
      title,
      description,
      date,
      files: filesArray,
      checked: false,
    })
    closeModal()
  }
 
  const addFiles = async (files) => {
    if (files.length < 1) return []
    for (let i = 0; i < files.length; i++) {
      const fileRef = ref(storage, `files/${files[i].name + v4()}`)
      
     await uploadBytes(fileRef, files[i]).then((snapshot) => {
        getDownloadURL(snapshot.ref).then((url) => {
          setFilesArray((prev) => [...prev, url.toString()])
        })
      })
    }
    return filesArray
  }

  const deleteFile = (url) => {
    setFilesArray((files) => files.filter(item => item !== url) )
  }

  return (
    <div className={styles.wrapper}>
      <div className={styles.body}>
        <div 
          className={styles.close}
          onClick={() => closeModal()}
        >x</div>
        <form className={styles.form} onSubmit={addTodo}>
          <input
            type="text"
            placeholder="New task"
            className={styles.title}
            onChange={(e) => setTitle(e.target.value)}
            value={title}
            required
          />
          <input
            type="datetime-local"
            className={styles.date}
            onChange={(e) => setDate(e.target.value)}
            value={date}
            required
          />
          <textarea
            required
            name="description"
            className={styles.description}
            placeholder="Description"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          />
          <input type="file" accept=".jpg, .jpeg, .png, .pdf" multiple onChange={(e) => addFiles(e.target.files)}/>
          <div className={styles.files}>
            {filesArray.length < 1 ? (<h4>No files added</h4>) : (<h4>Files</h4>)}
            {filesArray.map((item, index) => (
              <div key={item} className={styles.file}>
                <a href={item} rel="noreferrer" target="_blank">File {index + 1}</a>
                <button onClick={() => deleteFile(item)}>x</button>
              </div>
            ))}
          </div>
          <button className={styles.button} type="submit">
            Add task
          </button>
        </form>
      </div>
    </div>
  )
}
