import { ListItem } from 'components/ui/list-item'
import { Container } from 'components/ui/container'

import styles from './styles.module.css'

/**
 * Component for showing list of items
 *
 * @component
 * @example
 * return (
 *   <ListItem data={data} update={func} deleteTodo={func} />
 * )
 */

export const List = ({data, update, deleteTodo}) => {
  return (
    <div className={styles.wrapper}>
      <Container className={styles.container}>
        {
          data.length < 1 && <div className={styles.loading}>Loading...</div>
        }
        {
          data.map(item => <ListItem key={item.id} data={item} update={update} deleteTodo={deleteTodo} />)
        }
      </Container>
    </div>
  )
}