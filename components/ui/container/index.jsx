import cn from 'classnames'
import styles from './styles.module.css'

/**
 * Component for center position.
 *
 * @component
 * @example
 * return (
 *   <Container className={className}>children</Container> 
 * )
 */

export const Container = ({children, className}) => {  
  return (
    <div className={cn(styles.container, className)}>
      {children}
    </div>
  )
}