import { useState } from 'react'
import { ref, uploadBytes, getDownloadURL } from 'firebase/storage'
import dayjs from 'dayjs'
import { storage } from 'firebase-config'
import { v4 } from 'uuid'

import styles from './styles.module.css'

/**
 * Component for showing details of list item.
 *
 * @component
 * @example
 * return (
 *   <ListItem data={data} update={func} deleteTodo={func} />
 * )
 */

export const ListItem = ({ data, update, deleteTodo }) => {
  const [title, setTitle] = useState(data.title)
  const [description, setDescription] = useState(data.description)
  const [date, setDate] = useState(data.date)
  const [files, setFiles] = useState(data.files)
  const [checked, setChecked] = useState(data.checked)
  const [showMore, setShowMore] = useState(false)

  const handleUpdate = () => {
    update({
      id: data.id,
      title,
      description,
      date,
      files,
      checked,
    })
  }

  const addFiles = async (files) => {
    if (files.length < 1) return []
    for (let i = 0; i < files.length; i++) {
      const fileRef = ref(storage, `files/${files[i].name + v4()}`)

      await uploadBytes(fileRef, files[i]).then((snapshot) => {
        getDownloadURL(snapshot.ref).then((url) => {
          setFiles((prev) => [...prev, url.toString()])
        })
      })
    }
  }

  const deleteFile = (url) => {
    setFiles((files) => files.filter((item) => item !== url))
  }

  return (
    <div className={styles.item}>
      <div className={styles.close} onClick={() => deleteTodo(data)}>
        x
      </div>
      <div className={styles.row}>
        <input
          type="checkbox"
          checked={checked}
          onChange={() => setChecked(!checked)}
        />
        <input
          type="text"
          className={styles.title}
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <input
          type="datetime-local"
          className={styles.date}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
      </div>
      {dayjs(date).valueOf() < Date.now() && !checked && (
        <span>Time is up!</span>
      )}
      <button
        className={styles.showMore}
        onClick={() => setShowMore(!showMore)}
      >
        {!showMore ? 'show more' : 'show less'}
      </button>
      {showMore && (
        <>
          <textarea
            value={description}
            className={styles.description}
            onChange={(e) => setDescription(e.target.value)}
            name=""
          />
          <input
            type="file"
            accept=".jpg, .jpeg, .png, .pdf"
            multiple
            onChange={(e) => addFiles(e.target.files)}
          />
          <div className={styles.files}>
            {files.length < 1 ? <h4>No files added</h4> : <h4>Files</h4>}
            {files.map((item, index) => (
              <div key={item} className={styles.file}>
                <a href={item} rel="noreferrer" target="_blank">
                  File {index + 1}
                </a>
                <button onClick={() => deleteFile(item)}>x</button>
              </div>
            ))}
          </div>
        </>
      )}
      <button className={styles.button} onClick={handleUpdate}>
        Update
      </button>
    </div>
  )
}
