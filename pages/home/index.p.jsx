import { List } from 'components/common/list'
import { NewTodoForm } from 'components/shared/new-todo-form'
import { useState, useEffect } from 'react'
import { db } from 'firebase-config'
import {
  collection,
  getDocs,
  addDoc,
  updateDoc,
  doc,
  deleteDoc,
} from '@firebase/firestore'

import styles from './styles.module.css'

/**
 * Home Page
 * @component
 */

const HomePage = () => {
  const [todos, setTodos] = useState([])
  const [newTodoForm, setNewTodoForm] = useState(false)
  const todosCollectionRef = collection(db, 'todos')

  /**
 * create new todo
 * @param   {object} data data of todo
 * @return void
 */
  const createTodo = async (data) => {
    await addDoc(todosCollectionRef, data)
    setTodos(() => [data, ...todos])
  }

   /**
 * update todo
 * @param   {object} data data of todo
 * @return void
 */
  const updateTodo = async (data) => {
    const todoDoc = doc(db, 'todos', data.id)
    await updateDoc(todoDoc, data)
  }

   /**
 * delete todo
 * @param   {object} data data of todo
 * @return void
 */
  const deleteTodo = async (data) => {
    const todoDoc = doc(db, 'todos', data.id)
    await deleteDoc(todoDoc)
    setTodos((todos) => todos.filter((todo) => todo.id !== data.id))
  }

  useEffect(() => {
    const getTodos = async () => {
      const data = await getDocs(todosCollectionRef)
      setTodos(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    }

    getTodos()
  }, [todosCollectionRef])

  return (
    <div>
      <button className={styles.button} onClick={() => setNewTodoForm(true)}>
        Add new task
      </button>
      {newTodoForm && (
        <NewTodoForm
          closeModal={() => setNewTodoForm(false)}
          createTodo={createTodo}
        />
      )}
      <List data={todos} update={updateTodo} deleteTodo={deleteTodo} />
    </div>
  )
}

export default HomePage